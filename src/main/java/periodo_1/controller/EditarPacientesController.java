package periodo_1.controller;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import periodo_1.model.dao.PacientesDao;
import periodo_1.model.entity.Pacientes;
import periodo_1.model.enums.Convenios;
import periodo_1.model.util.ErroValidacaoUtils;
import periodo_1.model.util.JanelaUtils;
import periodo_1.model.util.Utils;
import periodo_1.model.util.ValidaCamposUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


/**
 * @author Igor Guilherme Almeida Rocha
 * Classe controller da view: editar_pacientes_view
 */
public class EditarPacientesController implements Initializable {

    @FXML
    public TextField tfNome;
    @FXML
    public TextField tfCpf;
    @FXML
    public TextField tfIdade;
    @FXML
    public TextField tfEndereco;

    @FXML
    public Button btAtualizar;
    @FXML
    public Button btCancelar;

    @FXML
    public ChoiceBox<Convenios> cbConvenios;

    private PacientesDao pacientesDao = new PacientesDao();
    private Pacientes paciente;


    private Long tempoUltTecla;

    public static final String EDITAR_PACIENTES_NOME = "editar_pacientes_view";

    public static final String EDITAR_PACIENTES_TITULO = "Editar informações pacientes";



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initPaciente();
        setConvenios();
        setEventos();

    }


    /**
     * Inicializa os campos com as informações atuais do paciente escolhido
     */
    private void initPaciente(){
        this.tfNome.setText(paciente.getNome());
        this.tfCpf.setText(paciente.getCpf());
        this.tfIdade.setText(paciente.getIdade()+"");
        this.tfEndereco.setText(paciente.getEndereco());
    }

    /**
     * Preenche a ChoiceBox com os convênios
     */
    private void setConvenios(){
        List<Convenios> conveniosList = new ArrayList<>();
        conveniosList.addAll(Arrays.asList(Convenios.HAPVIDA, Convenios.SULAMERICA, Convenios.NOTREDAME, Convenios.BRADESCO, Convenios.AMIL));
        cbConvenios.setItems(FXCollections.observableArrayList(conveniosList));
        cbConvenios.setValue(this.paciente.getConvenio());
    }

    /**
     * Inicializa os eventos relacionados a componentes da tela
     */
    private void setEventos(){
        setTfEventos();
        setBtEventos();
    }

    /**
     * Inicializa os eventos dos botões
     */
    private void setBtEventos(){
        this.btAtualizar.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Utils.limpaEstilo(Arrays.asList(tfNome, tfCpf, tfIdade, tfEndereco));
                        if(ValidaCamposUtils.validaCampos(Arrays.asList(tfNome, tfCpf, tfIdade, tfEndereco), tfIdade.getText(), tfCpf.getText(), cbConvenios)){
                            atualizaPaciente();
                            Integer queryResult = pacientesDao.updatePaciente(paciente);
                            if(queryResult == null) JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CONEXAO_BANCO);
                            else if(queryResult == - 1) JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CPF_DUPLICADO);
                            else JanelaUtils.fechaJanela(event);
                        }
                    }
                }
        );

        this.btCancelar.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        JanelaUtils.fechaJanela(event);
                    }
                }
        );
    }

    /**
     * Inicializa os eventos das caixas de texto
     */
    private void setTfEventos(){
        this.tfCpf.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent event) {
                        Boolean eBackSpace = ValidaCamposUtils.verificaBackSpace(tfCpf, event);
                        if(!eBackSpace) ValidaCamposUtils.verificaMascaraCpf(tfCpf, tfCpf.getText(), event.getText(),ValidaCamposUtils.verificaTempo(tempoUltTecla));
                        tempoUltTecla = System.currentTimeMillis();
                    }
                }
        );
    }

    /**
     * Atualiza o paciente com as novas informações digitadas pelo usuário
     */
    private void atualizaPaciente(){
        this.paciente.setNome(this.tfNome.getText());
        this.paciente.setCpf(this.tfCpf.getText());
        this.paciente.setIdade(Integer.valueOf(this.tfIdade.getText()));
        this.paciente.setEndereco(this.tfEndereco.getText());
        this.paciente.setConvenio(this.cbConvenios.getSelectionModel().getSelectedItem());
    }


    public void setPaciente(Pacientes paciente){
        this.paciente = paciente;
    }


}
