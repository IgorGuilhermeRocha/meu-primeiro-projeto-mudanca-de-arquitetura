package periodo_1.controller;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import periodo_1.model.dao.PacientesDao;
import periodo_1.model.entity.Pacientes;
import periodo_1.model.enums.Convenios;
import periodo_1.model.util.JanelaUtils;
import periodo_1.model.util.ErroValidacaoUtils;
import periodo_1.model.util.Utils;
import periodo_1.model.util.ValidaCamposUtils;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe controller da view: cadastro_pacientes_view
 */
public class CadastroPacientesController implements Initializable {

    public static final String CADASTRO_PACIENTES_NOME = "cadastro_pacientes_view";
    public static final String CADASTRO_PACIENTES_TITULO = "Cadastro pacientes";

    private final Integer NOME_TAMANHO = 50;
    private final Integer IDADE_TAMANHO = 3;
    private final Integer ENDERECO_TAMANHO = 100;

    private Long tempoUltTecla;

    private PacientesDao pacientesDao =  new PacientesDao();
    private Pacientes pacientes;

    @FXML
    public TextField tfNome;
    @FXML
    public TextField tfCpf;
    @FXML
    public TextField tfIdade;
    @FXML
    public TextField tfEndereco;
    @FXML
    public Button btCadastro;

    @FXML
    public Button btCancelar;
    @FXML
    public ChoiceBox<Convenios> cbConvenios;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setConvenios();
        setEventos();
        modificaTamanhoMaximoDosCampos();

    }

    /**
     * Preenche a ChoiceBox de convẽnios
     */
    private void setConvenios(){
        cbConvenios.setItems(FXCollections.observableArrayList(Convenios.values()));
    }

    /**
     * inicializa os eventos relacionados com os componentes da tela
     */
    private void setEventos(){
        setTfEventos();
        setBtEventos();
    }

    /**
     * Inicializa os eventos das caixas de texto
     */
    private void setTfEventos(){
        this.tfCpf.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                Boolean eBackSpace = ValidaCamposUtils.verificaBackSpace(tfCpf, event);
                if(!eBackSpace) ValidaCamposUtils.verificaMascaraCpf(tfCpf, tfCpf.getText(), event.getText(),ValidaCamposUtils.verificaTempo(tempoUltTecla));
                tempoUltTecla = System.currentTimeMillis();
            }
        });

        modificaTamanhoMaximoDosCampos();
    }


    /**
     * Inicializa os eventos dos botões
     */
    private void setBtEventos(){
        this.btCadastro.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Utils.limpaEstilo(Arrays.asList(tfNome, tfCpf, tfIdade, tfEndereco));
                        if(ValidaCamposUtils.validaCampos(Arrays.asList(tfNome, tfCpf, tfIdade, tfEndereco), tfIdade.getText(), tfCpf.getText(), cbConvenios)){
                            constroiPaciente();
                            Integer queryResult = pacientesDao.cadastroPaciente(pacientes);
                            if(queryResult == null) JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CONEXAO_BANCO);
                            else if(queryResult == - 1) JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CPF_DUPLICADO);
                            else JanelaUtils.fechaJanela(event);
                        }


                    }
                }
        );

        this.btCancelar.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        JanelaUtils.fechaJanela(event);
                    }
                }
        );


    }

    /**
     * Modifica o tamanho máximo dos campos de texto
     */
    private void modificaTamanhoMaximoDosCampos(){
        ValidaCamposUtils.adicionaLimiteDeTexto(this.tfNome, NOME_TAMANHO);
        ValidaCamposUtils.adicionaLimiteDeTexto(this.tfIdade, IDADE_TAMANHO);
        ValidaCamposUtils.adicionaLimiteDeTexto(this.tfEndereco, ENDERECO_TAMANHO);
    }

    /**
     * Utiliza as informações digitadas pelo usuário, para construir uma instância de paciente
     */
    private void constroiPaciente(){
        this.pacientes = new Pacientes(tfNome.getText(),
                tfCpf.getText(),
                Integer.valueOf(tfIdade.getText()),
                tfEndereco.getText(), this.cbConvenios.getSelectionModel().getSelectedItem());
    }



}
