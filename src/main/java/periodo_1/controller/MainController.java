package periodo_1.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import periodo_1.model.dao.ConsultasDao;
import periodo_1.model.dao.PacientesDao;
import periodo_1.model.entity.Consultas;
import periodo_1.model.entity.Pacientes;
import periodo_1.model.util.ErroValidacaoUtils;
import periodo_1.model.util.JanelaUtils;
import periodo_1.model.util.Utils;
import periodo_1.model.util.ValidaCamposUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe controller da view: main_view
 */
public class MainController implements Initializable {


    @FXML
    public Button btCadastrar;

    @FXML
    public Button btEditarPacientes;

    @FXML
    public Button btMarcarConsulta;

    @FXML
    public Button btRelatorio;

    @FXML
    public TextField tfCpf;

    private Long tempoUltTecla;

    private PacientesDao pacientesDao = new PacientesDao();
    private ConsultasDao consultasDao = new ConsultasDao();

    private static final String CAMINHO_RELATORIO = "Pacientes.txt";


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setEvents();
    }

    /**
     * Inicializa os eventos relacionados com componentes de tela
     */
    private void setEvents(){
        setBtEvents();
        setTfEvents();
    }

    /**
     * Inicializa os eventos relacionados a botões
     */
    private void setBtEvents(){
        this.btCadastrar.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            mudarTela(JanelaUtils.eventParaStage(event),
                                    CadastroPacientesController.CADASTRO_PACIENTES_NOME,
                                    CadastroPacientesController.CADASTRO_PACIENTES_TITULO);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        this.btEditarPacientes.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Utils.limpaEstilo(Arrays.asList(tfCpf));
                            if(!ValidaCamposUtils.verificaCamposVazios(Arrays.asList(tfCpf)) && ValidaCamposUtils.validaCpf(tfCpf.getText())){
                                Pacientes paciente =  getPacientePorCpf();
                                if(paciente != null) {
                                    if(paciente.getId() != null) {
                                        EditarPacientesController editarPacientesController = new EditarPacientesController();
                                        editarPacientesController.setPaciente(paciente);
                                        mudarTela(JanelaUtils.eventParaStage(event), EditarPacientesController.EDITAR_PACIENTES_NOME, EditarPacientesController.EDITAR_PACIENTES_TITULO, editarPacientesController);
                                    }else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_USUARIO_NAO_CADASTRADO);
                                }else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CONEXAO_BANCO);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        this.btMarcarConsulta.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            mudarTela(JanelaUtils.eventParaStage(event),
                                    MarcarConsultaController.MARCAR_CONSULTA_NOME,
                                    MarcarConsultaController.MARCAR_CONSULTA_TITULO);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
        );

        this.btRelatorio.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        geraRelatorio();
                    }
                }
        );
    }

    /**
     * Inicializa os eventos relacionados com as caixas de texto
     */
    private void setTfEvents(){
        this.tfCpf.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent event) {
                        Boolean eBackSpace = ValidaCamposUtils.verificaBackSpace(tfCpf, event);
                        if(!eBackSpace) ValidaCamposUtils.verificaMascaraCpf(tfCpf, tfCpf.getText(), event.getText(), ValidaCamposUtils.verificaTempo(tempoUltTecla));
                        tempoUltTecla = System.currentTimeMillis();
                    }
                }
        );
    }

    /**
     * Cria uma próxima janela
     * @param stageAtual Janela atual
     * @param nomeTela Nome do arquivo fxml da janela a ser aberta
     * @param tituloTela Titulo da nova janela
     * @throws IOException
     */
    public void mudarTela(Stage stageAtual, String nomeTela, String tituloTela) throws IOException{
        JanelaUtils.createNewWindow(new Stage(),stageAtual, nomeTela, tituloTela);
    }

    /**
     * Cria uma próxima janela
     * @param stageAtual Janela atual
     * @param nomeTela Nome do arquivo fxml da janela a ser aberta
     * @param tituloTela Titulo da nova janela
     * @param controller Classe controladora da próxima janela
     * @throws IOException
     */
    public void mudarTela(Stage stageAtual, String nomeTela, String tituloTela, Object controller) throws IOException{
        JanelaUtils.createNewWindow(new Stage(),stageAtual, nomeTela, tituloTela, controller);
    }

    /**
     * Pega um paciente por seu Cpf
     * @return Uma instância de Pacientes
     */
    private Pacientes getPacientePorCpf(){
        return pacientesDao.verificaCpfCadastrado(this.tfCpf.getText());
    }

    private void geraRelatorio(){
        List<Consultas> consultasList = consultasDao.selecionaConsultas();
        if(consultasList != null){
            if(consultasList.size() > 0){
                try(BufferedWriter bf = new BufferedWriter(new FileWriter(CAMINHO_RELATORIO))){
                    for(Consultas consulta : consultasList ){
                        bf.write("Consulta dia: " + consulta.getDia().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+ " ");
                        bf.write("Horário: " + consulta.getHorario() + " ");
                        Pacientes paciente = pacientesDao.achaPorId(consulta.getIdPaciente());
                        bf.write("Paciente: " + paciente.getNome() + ", " + paciente.getCpf());
                        bf.newLine();

                    }
                }catch (IOException e){

                }
            }
        }

    }




}
