package periodo_1.controller;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import periodo_1.model.dao.ConsultasDao;
import periodo_1.model.dao.PacientesDao;
import periodo_1.model.entity.Consultas;
import periodo_1.model.entity.Pacientes;
import periodo_1.model.enums.Horarios;
import periodo_1.model.util.ErroValidacaoUtils;
import periodo_1.model.util.JanelaUtils;
import periodo_1.model.util.ValidaCamposUtils;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class MarcarConsultaController implements Initializable {

    public static final String MARCAR_CONSULTA_NOME = "marcar_consulta_view";

    public static final String MARCAR_CONSULTA_TITULO = "Marcar consulta";


    @FXML
    public DatePicker dpData;

    @FXML
    public ChoiceBox<Horarios> cbHorarios;

    @FXML
    public TextField tfCpf;

    @FXML
    public Button btConfirmar;

    @FXML
    public Button btCancelar;

    @FXML
    public Button btCarregarHorarios;

    private ConsultasDao consultasDao =  new ConsultasDao();

    private PacientesDao pacientesDao = new PacientesDao();

    private Long tempoUltTecla;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setEventos();
        dpData.setValue(LocalDate.now());
    }

    private void setEventos(){
        setDpEventos();
        setTfEventos();
        setBtEventos();
    }
    private void setDpEventos(){
        dpData.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                setDisable(empty || date.compareTo(today) < 0 );
            }
        });

        dpData.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("dsdsad");
                    }
                }
        );
    }

    private void setTfEventos(){
        this.tfCpf.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent event) {
                        Boolean eBackSpace = ValidaCamposUtils.verificaBackSpace(tfCpf, event);
                        if(!eBackSpace) ValidaCamposUtils.verificaMascaraCpf(tfCpf, tfCpf.getText(), event.getText(), ValidaCamposUtils.verificaTempo(tempoUltTecla));
                        tempoUltTecla = System.currentTimeMillis();
                    }
                }
        );
    }


    private void setBtEventos(){
        this.btCarregarHorarios.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        getHorariosValidos(consultasDao.selectConsultasPorData(dpData.getValue()));

                    }
                }
        );

        this.btCancelar.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        JanelaUtils.fechaJanela(event);
                    }
                }
        );

        this.btConfirmar.setOnMouseClicked(
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    marcaConsulta();
                    limpaChoiceBox();
                }
            }
        );
    }

    private void getHorariosValidos(List<Consultas> consultaslist){
        List<Horarios> horariosValidos = new ArrayList<>();
        horariosValidos.addAll(List.of(Horarios.values()));

        List<Horarios> todosHorarios = List.copyOf(horariosValidos);

        for(Consultas consulta : consultaslist){
            if(todosHorarios.contains(consulta.getHorario())){
                horariosValidos.remove(consulta.getHorario());
            }
        }

        setHorarios(horariosValidos);

    }

    private void setHorarios(List<Horarios> horariosList){
        this.cbHorarios.setItems(FXCollections.observableArrayList(horariosList));
    }

    private Boolean verificaCpfValido(){

        if (!ValidaCamposUtils.verificaCamposVazios(Arrays.asList(tfCpf))) {
            if (ValidaCamposUtils.validaCpf(tfCpf.getText())) return true;
            else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CPF);
        }
        return false;
    }

    private Long  verificaCpfCadastrado(){
        Pacientes paciente = pacientesDao.verificaCpfCadastrado(tfCpf.getText());
        if(paciente != null){
            if(paciente.getId() != null) return paciente.getId();
            else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_USUARIO_NAO_CADASTRADO);
        } else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CONEXAO_BANCO);
        return - 1l;
    }

    private void marcaConsulta(){
        if(cbHorarios.getValue() != null){
            if(verificaCpfValido()){
                Long idPaciente = verificaCpfCadastrado();
                if(idPaciente != null) consultasDao.marcaConsulta(criaConsulta(idPaciente));
                else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_USUARIO_NAO_CADASTRADO);
            }
        } else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_HORARIO);
    }

    private void limpaChoiceBox(){
        this.cbHorarios.setItems(FXCollections.observableArrayList(new ArrayList<>()));
    }

    private Consultas criaConsulta(Long idPaciente){
       return new Consultas(idPaciente, this.dpData.getValue(), this.cbHorarios.getSelectionModel().getSelectedItem());
    }

}
