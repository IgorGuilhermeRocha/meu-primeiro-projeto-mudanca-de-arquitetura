package periodo_1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import periodo_1.model.util.JanelaUtils;

import java.io.IOException;

/**
 * Ponto de entrada da aplicação
 * @Autor Igor Guilherme Almeida Rocha
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(JanelaUtils.loadFXML("main_view"));
        stage.setResizable(false);
        stage.setScene(scene);
        stage.setTitle("Sistema hospital");
        stage.show();
    }


    public static void main(String[] args) {
        launch();
    }
}
