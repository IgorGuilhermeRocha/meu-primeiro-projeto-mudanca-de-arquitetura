package periodo_1.model.util;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import periodo_1.model.enums.Convenios;

import java.util.List;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe utilitária para ajudar na validação dos campos, dos formulários.
 */
public class ValidaCamposUtils {

    private static final Integer CPF_PONTO_DE_QUEBRA_1 = 4;
    private static final Integer CPF_PONTO_DE_QUEBRA_2 = 8;
    private static final Integer CPF_PONTO_DE_QUEBRA_3 = 12;
    private static final Integer CPF_TAMANHO = 15;

    private static final Long TEMPO_DIGITO_CPF = 150L;


    /**
     * valida o formulário
     * @param campos todos os campos do formulario de cadastro de pacientes
     * @param idade idade digitada pelo usuario
     * @param cpf cpf digitado pelo usuario
     * @param cbConvenios ChoiceBox<Convenios>
     * @return true se todos os campos estiverem no padrao correto, false caso o contrário
     */
    public static Boolean validaCampos(List<TextField> campos, String idade, String cpf,  ChoiceBox<Convenios> cbConvenios){
        if(!ValidaCamposUtils.verificaCamposVazios(campos)){
            if(ValidaCamposUtils.validaIdade(idade)) {
                if(ValidaCamposUtils.validaCpf(cpf)){
                    if(ValidaCamposUtils.validaConvenios(cbConvenios)){
                        return true;
                    } else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CONVENIO);
                } else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_CPF);
            }else JanelaUtils.janelaErro(ErroValidacaoUtils.ERRO_IDADE);
        }
        return false;
    }

    /**
     * pattern = 000.000.000-00
     * @param cpf o cpf a ser validado
     * @return  true se estiver no formato correto, false caso o contrario
     */
    public static Boolean validaCpf(String cpf){
        return cpf.matches("([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})");
    }

    /**
     * @param idade idade a ser validada
     * @return true se idade for um número inteiro >= 0
     */
    private static Boolean validaIdade(String idade){
        Boolean eNumero = Utils.stringParaInteiro(idade);
        if(eNumero) return Integer.valueOf(idade) >= 0;
        else return false;
    }

    /**
     * @param cbConvenios Convenio a ser validado
     * @return true se o item selecionado for diferente de null
     */
    private static Boolean validaConvenios(ChoiceBox<Convenios> cbConvenios){
        return cbConvenios.getSelectionModel().getSelectedItem() != null;
    }

    /**
     * Muda a cor da borda dos campos vazios para vermelho
     * @param fields campos a serem validados
     * @return true se algum campo estiver vazio
     */
    public static Boolean verificaCamposVazios(List<TextField> fields){
        for(TextField field : fields){
            if(field.getText().isBlank()) {
                field.setStyle("-fx-text-box-border: red ; -fx-focus-color: red ;");
                return true;
            }
        }
        return false;
    }

    /**
     * ajuda o usuário a formatar o cpf
     * @param tfCpf objeto de campo que contem o cpf
     * @param texto texto atual
     * @param tecla tecla digitada
     * @param muitoRapido tempo de diferença entre as duas ultimas teclas
     */
    public static void verificaMascaraCpf(TextField tfCpf, String texto, String tecla, boolean muitoRapido){
        boolean maisUmCaractere = true;

        if((!tecla.equals("") && !tecla.matches("[0-9]") || texto.length() >= CPF_TAMANHO) || muitoRapido){
            tfCpf.setText(removeUltimoCaractere(texto));
            maisUmCaractere = false;
        }else if(tecla.equals("")){
            maisUmCaractere = false;
        }else if(texto.length() == CPF_PONTO_DE_QUEBRA_1|| texto.length() == CPF_PONTO_DE_QUEBRA_2){
            tfCpf.setText(adicionaCaractereDeQuebra(texto, '.'));
        }else if(texto.length() == CPF_PONTO_DE_QUEBRA_3){
            tfCpf.setText(adicionaCaractereDeQuebra(texto, '-'));
        }
        if(!maisUmCaractere) tfCpf.positionCaret(texto.length());
        else tfCpf.positionCaret(texto.length() + 1);

    }

    /**
     *
     * @param texto texto a ser modificado
     * @return Uma string contendo o texto recebido menos a ultima letra
     */
    private static String removeUltimoCaractere(String texto){
        return texto.substring(0, texto.length() - 1);
    }

    /**
     * Ex : texto -> 4444, retorno -> 444.4
     * @param texto texto a ser formatado
     * @param caractere caractere a ser colocado no texto
     * @return retorna o texto formatado
     */
    private static String adicionaCaractereDeQuebra(String texto, Character caractere){
        return texto.substring(0, texto.length() - 1) + caractere + texto.charAt(texto.length() - 1);
    }

    /**
     * @param tfCpf campo de texto do cpf
     * @param event evento
     * @return true se a tecla digitada for o back_space
     */
    public static Boolean verificaBackSpace(TextField tfCpf, KeyEvent event){
        String textoCpf = tfCpf.getText();
        if(event.getCode() == KeyCode.BACK_SPACE){
            if(textoCpf.charAt(textoCpf.length() - 1) == '.' || textoCpf.charAt(textoCpf.length() - 1) == '-' ){
                tfCpf.setText(removeUltimoCaractere(textoCpf));
            }
            tfCpf.positionCaret(tfCpf.getText().length());
            return true;
        }
        return false;

    }

    /**
     * verifica tempo da ultima tecla pressionada e o tempo atual
     * @param tempoUltTecla tempo em que a última tecla foi pressionada
     * @return true se o tempo da ultima tecla <= TEMPO_DIGITO_CPF
     */

    public static Boolean verificaTempo(Long tempoUltTecla){
        if(tempoUltTecla == null){
            return false;
        }else{
            long tempoAtual = System.currentTimeMillis();
            long tempoUltTeclaTemp = tempoUltTecla;
            if(tempoAtual - tempoUltTeclaTemp <= TEMPO_DIGITO_CPF) return true;
        }
        return false;
    }


    /**
     * modifica o tamanho máximo do texto que pode ser digitado no campo
     * @param tf campo a ser modificado
     * @param tamanhoMaximo tamanho máximo do campo
     */
    public static void adicionaLimiteDeTexto(TextField tf, final int tamanhoMaximo) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                if (tf.getText().length() > tamanhoMaximo) {
                    String s = tf.getText().substring(0, tamanhoMaximo);
                    tf.setText(s);
                }
            }
        });
    }





}
