package periodo_1.model.util;

import javafx.scene.control.TextField;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe utilitária genêrica
 */
public class Utils {

    /**
     * verifica se a string pode ser um npumero
     * @param idade uma string representando uma idade
     * @return true se a idade puder ser convertida para um número
     */
    public static Boolean stringParaInteiro(String idade){
        try{
            Integer.valueOf(idade);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }


    /**
     * limpa os estilos dos TextFields passados como parâmetro
     * @param campos Uma lista de TextFields
     */
    public static void limpaEstilo(List<TextField> campos){
        for(TextField campo : campos){
            campo.setStyle("");
        }
    }

    /**
     * Lê o arquivo do caminho passado como parâmetro.
     * @param path caminho do arquivo
     * @return Objeto Properties
     * @throws IOException caso o caminho esteja incorreto, ou algum outro erro relacionado
     */
    public static Properties loadProperties(String path) throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream(path);
        props.load(file);
        return props;

    }

}
