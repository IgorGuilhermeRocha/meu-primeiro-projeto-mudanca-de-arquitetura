package periodo_1.model.util;

import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.stage.Stage;
import periodo_1.App;
import java.io.IOException;

public class JanelaUtils {


    /**
     * Cria uma nova janela
     * @param stage Stage onde vai ser carregada a view
     * @param stageOwner Stage "pai" dessa nova janela
     * @param viewName nome da view
     * @param title titulo da janela
     * @throws IOException
     */
    public static void createNewWindow(Stage stage, Stage stageOwner, String viewName, String title)
            throws IOException {
        Scene scene = new Scene(loadFXML(viewName));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(stageOwner);
        stage.setTitle(title);
        stage.show();
    }

    /**
     * Cria uma nova janela
     * @param stage Stage onde vai ser carregada a view
     * @param stageOwner Stage "pai" dessa nova janela
     * @param viewName nome da view
     * @param title titulo da janela
     * @param controller Classe controladora desta view
     * @throws IOException
     */
    public static void createNewWindow(Stage stage, Stage stageOwner, String viewName, String title, Object controller)
            throws IOException {
        Scene scene = new Scene(loadFXML(viewName, controller));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(stageOwner);
        stage.setTitle(title);
        stage.show();
    }

    /**
     * Carrega o component raiz para a criação da janela
     * @param fxml Nome do arquivo fxml
     * @return Um parent
     * @throws IOException
     */

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Carrega o component raiz para a criação da janela
     * @param fxml Nome do arquivo fxml
     * @param controller Classe controladora dd view
     * @return Um parent
     * @throws IOException
     */
    public static Parent loadFXML(String fxml, Object controller) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/" + fxml + ".fxml"));
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }

    /**
     * Converte um evento para um stage
     * @param event Um evento
     * @return Um Stage onde foi disparado este evento
     */
    public static Stage eventParaStage(Event event){
         return  (Stage) ((Node) event.getSource()).getScene().getWindow();
    }


    /**
     * Fecha a janela em que foi disparado o evento
     * @param event evento disparado pelo usuário
     */
    public static void fechaJanela(Event event){
        eventParaStage(event).close();
    }



    /**
     * Cria janela de erro
     * @param bodyText Texto que vai ser exibido para o usuário
     */
    public static void janelaErro(String bodyText) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erro");
        alert.setHeaderText("");
        alert.setContentText(bodyText);
        alert.show();

    }




}
