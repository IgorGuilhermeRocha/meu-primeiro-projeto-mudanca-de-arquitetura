package periodo_1.model.util;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe utilitária contendo todas as mensagens de erro padrão.
 */
public class ErroValidacaoUtils {

    public static final String ERRO_CPF = "Cpf inválido, o cpf deve ser composto de 11 números, no formato 000.000.000-00";
    public static final String ERRO_IDADE = "Idade inválida, idade deve ser um valor inteiro positivo.";
    public static final String ERRO_CONVENIO = "Por favor, selecione um convênio.";

    public static final String ERRO_CPF_DUPLICADO = "Cpf já cadastrado no sistema";
    public static final String ERRO_USUARIO_NAO_CADASTRADO = "Paciente não cadastrado";
    public static final String ERRO_HORARIO = "Por favor, selecione um horário";
    public static final String ERRO_CONEXAO_BANCO = "O servidor esta fora dor ar, tente novamente mais tarde";
}
