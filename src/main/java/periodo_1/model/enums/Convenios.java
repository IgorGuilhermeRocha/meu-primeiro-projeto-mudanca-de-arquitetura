package periodo_1.model.enums;

/**
 * @Autor Igor Guilherme Almeida Rocha
 */
public enum Convenios {

    AMIL("Amil Assistência Médica Internacional"),
    BRADESCO("Bradesco Saúde"),
    NOTREDAME("NotreDame"),
    SULAMERICA("SulAmérica"),
    HAPVIDA("HapVida");

    private String nome;

    private Convenios(String nome){
        this.nome = nome;
    }

    public static Convenios getConvenio(String nome){
       for(Convenios convenio : Convenios.values()){
           if(convenio.toString().equals(nome)){
               return convenio;
           }
       }
       return null;
   }

    @Override
    public String toString() {
        return this.nome;
    }
}
