package periodo_1.model.enums;

import java.time.LocalTime;

/**
 * @Autor Igor Guilherme Almeida Rocha
 */
public enum Horarios {

    OITO(LocalTime.of(8,0)),
    NOVE(LocalTime.of(9,0)),
    DEZ(LocalTime.of(10,0)),
    ONZE(LocalTime.of(11,0)),
    TREZE(LocalTime.of(13,0)),
    QUATORZE(LocalTime.of(14,0)),
    QUINZE(LocalTime.of(15,0)),
    DEZESSEIS(LocalTime.of(16,0));
    private LocalTime hora;

    private Horarios(LocalTime hora){
        this.hora = hora;
    }

    public static Horarios getHorario(LocalTime hora){
        for(Horarios horario : Horarios.values()){
            if(horario.toString().equals(hora.toString())){
                return horario;
            }
        }
        return null;
    }
    @Override
    public String toString() {
        return hora+"";
    }
}
