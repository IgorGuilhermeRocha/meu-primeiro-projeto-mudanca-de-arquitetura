package periodo_1.model.entity;

import periodo_1.model.enums.Horarios;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @Autor Igor Guilherme Almeida Rocha
 */
public class Consultas {

    private Long id;

    private Long idPaciente;

    private LocalDate dia;
    private Horarios horario;

    public Consultas(){

    }

    public Consultas(Long id, Long idPaciente, LocalDate dia, Horarios horario) {
        this.id = id;
        this.idPaciente = idPaciente;
        this.dia = dia;
        this.horario = horario;
    }
    public Consultas(Long idPaciente, LocalDate dia, Horarios horario) {
        this.idPaciente = idPaciente;
        this.dia = dia;
        this.horario = horario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public LocalDate getDia() {
        return dia;
    }

    public void setDia(LocalDate dia) {
        this.dia = dia;
    }

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consultas consultas = (Consultas) o;
        return Objects.equals(id, consultas.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
