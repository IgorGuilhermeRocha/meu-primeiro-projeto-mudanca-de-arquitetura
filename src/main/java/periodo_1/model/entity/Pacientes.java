package periodo_1.model.entity;

import periodo_1.model.enums.Convenios;

import java.util.Objects;

/**
 * @Autor Igor Guilherme Almeida Rocha
 */
public class Pacientes {

    private Long id;
    private String nome;
    private String cpf;
    private Integer idade;
    private String endereco;

    private Convenios convenio;

    public Pacientes(){

    }

    public Pacientes(String nome, String cpf){
        this.nome = nome;
        this.cpf = cpf;
    }
    public Pacientes(String nome, String cpf, Integer idade, String endereco, Convenios convenio) {
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
        this.endereco = endereco;
        this.convenio = convenio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Convenios getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenios convenio) {
        this.convenio = convenio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pacientes pacientes = (Pacientes) o;
        return cpf.equals(pacientes.cpf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpf);
    }

    @Override
    public String toString() {
        return "Pacientes{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", idade=" + idade +
                ", endereco='" + endereco + '\'' +
                ", convenio=" + convenio +
                '}';
    }
}
