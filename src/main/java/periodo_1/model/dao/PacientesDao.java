package periodo_1.model.dao;
import java.sql.*;

import periodo_1.model.entity.Pacientes;
import periodo_1.model.enums.Convenios;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe reponsável por fazer as consultas relacionadas a entidade Pacientes no banco de dados.
 */
public class PacientesDao {

    public PacientesDao() {

    }

    private Connection conexao;
    private String sql;

    /**
     * Insere paciente no banco
     * @param paciente Paciente a ser inserido
     * @return um inteiro que corresponse ao sucesso da inserção
     */
    public Integer cadastroPaciente(Pacientes paciente) {

        this.sql = "INSERT INTO pacientes VALUES (default, ? , ? , ? , ?, ?)";

        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = conexao.prepareStatement(sql);
            ps.setString(1, paciente.getNome());
            ps.setString(2, paciente.getCpf());
            ps.setInt(3, paciente.getIdade());
            ps.setString(4, paciente.getEndereco());
            ps.setString(5, paciente.getConvenio().toString());
            int result = ps.executeUpdate();
            return result;

        } catch (SQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            return -1;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                return null;
            }
        }
    }


    /**
     * Retorna o paciente com este cpf, se existir
     * @param cpf Cpf do paciente
     * @return Um Paciente
     */
    public Pacientes verificaCpfCadastrado(String cpf){

        this.sql = "SELECT id, nome, cpf, idade, endereco, convenio FROM pacientes as p WHERE p.cpf = ?";

        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = conexao.prepareStatement(this.sql);
            ps.setString(1, cpf);
            ResultSet queryResult = ps.executeQuery();
            Pacientes paciente = new Pacientes();

            if(queryResult.next()){
                paciente.setId(queryResult.getLong("id"));
                paciente.setNome(queryResult.getNString("nome"));
                paciente.setCpf(queryResult.getNString("cpf"));
                paciente.setIdade(queryResult.getInt("idade"));
                paciente.setEndereco(queryResult.getNString("endereco"));
                paciente.setConvenio(Convenios.getConvenio(queryResult.getNString("convenio")));
            }
            return paciente;

        }  catch (SQLException e) {
            e.printStackTrace();
            return null;

        }  finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                return null;
            }
        }

    }

    /**
     * Atualiza as informações de um determinado paciente
     * @param paciente
     * @return Um inteiro correspondente ao resultado da operação
     */
    public Integer updatePaciente(Pacientes paciente){
        this.sql = "UPDATE pacientes SET nome = ?, cpf = ?, idade = ?, endereco = ?, convenio = ? " +
                "WHERE id = ?";

        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = conexao.prepareStatement(this.sql);
            ps.setString(1, paciente.getNome());
            ps.setString(2, paciente.getCpf());
            ps.setInt(3, paciente.getIdade());
            ps.setString(4, paciente.getEndereco());
            ps.setString(5, paciente.getConvenio().toString());
            ps.setLong(6, paciente.getId());

            return ps.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e){
            e.printStackTrace();
            return -1;
        } catch (Exception e){
            e.printStackTrace();
        }  finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                return null;
            }
        }

        return null;
    }

    /**
     * Seleciona o paciente pelo seu id
     * @return Um paciente
     */
    public Pacientes achaPorId(Long idPaciente){
        this.sql = "SELECT nome, cpf FROM pacientes WHERE id = ?";
        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = this.conexao.prepareStatement(this.sql);
            ps.setLong(1, idPaciente);

            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                return new Pacientes(rs.getNString("nome"), rs.getNString("cpf"));
            }
            return new Pacientes();


        }catch (SQLException e){
            e.printStackTrace();

        }finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

}
