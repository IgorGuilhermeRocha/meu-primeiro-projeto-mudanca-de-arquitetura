package periodo_1.model.dao;
import periodo_1.model.util.Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Properties;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe responsável por gerênciar as instâncias de conexão com o banco
 */
public class CriaConexaoComBanco {

    private static Connection conn = null;

    /**
     * Retorna conexão com o banco
     * @return Connection
     * @throws SQLException
     */
    public static Connection criaConexaoComBanco() throws SQLException {
        if (conn == null || conn.isClosed() == true) {
            try {
                Properties properties = Utils.loadProperties("src/main/resources/periodo_1/database/db.properties");
                String user = properties.getProperty("user");
                String password = properties.getProperty("password");
                String dburl = properties.getProperty("dburl");
                conn = DriverManager.getConnection(dburl, user, password);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }
        return conn;

    }

}
