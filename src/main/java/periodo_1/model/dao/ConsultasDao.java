package periodo_1.model.dao;

import periodo_1.model.entity.Consultas;
import periodo_1.model.enums.Horarios;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @Autor Igor Guilherme Almeida Rocha
 * Classe reponsável por fazer as consultas relacionadas a entidade Consultas no banco de dados.
 */
public class ConsultasDao {

    private Connection conexao;

    private String sql;

    public ConsultasDao(){

    }

    /**
     * lista todas as consultas
     * @return uma lista de consultas
     */
    public List<Consultas> selecionaConsultas() {
        this.sql = "SELECT * FROM consultas;";

        try{
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            ResultSet rs =  conexao.prepareStatement(this.sql).executeQuery();
            List<Consultas> consultasList =  new ArrayList<>();

            while (rs.next()){
                consultasList.add(new Consultas(rs.getLong("id"),
                        rs.getLong("id_paciente"),
                        LocalDate.parse(rs.getDate("dia").toString()),
                        Horarios.getHorario(LocalTime.parse(rs.getString("horario")))));
            }
            return consultasList;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    /**
     * Insere uma consulta no banco
     * @param consulta Uma Consulta
     * @return um inteiro relacionado com o resultado da operação
     */
    public Integer marcaConsulta(Consultas consulta){
        this.sql = "INSERT INTO consultas VALUES(default, ?, ?, ?)";

        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = conexao.prepareStatement(this.sql);
            ps.setDate(1, Date.valueOf(consulta.getDia()));
            ps.setNString(2, consulta.getHorario().toString());
            ps.setLong(3, consulta.getIdPaciente());

            return ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        }finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Retorna uma lista de consultas que estão marcadas para a data especificada
     * @param data Uma data
     * @return Uma lista de consultas
     */
    public List<Consultas> selectConsultasPorData(LocalDate data){

        this.sql = "SELECT * FROM consultas WHERE dia = ?";
        List<Consultas> consultasList = new ArrayList<>();

        try {
            this.conexao = CriaConexaoComBanco.criaConexaoComBanco();
            PreparedStatement ps = conexao.prepareStatement(this.sql);
            ps.setDate(1, Date.valueOf(data));
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                consultasList.add(new Consultas(rs.getLong("id"),
                        rs.getLong("id_paciente"),
                        LocalDate.parse(rs.getDate("dia").toString()),
                        Horarios.getHorario(LocalTime.parse(rs.getNString("horario")))));

            }

            return consultasList;

        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                this.conexao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
