module periodo {
    requires javafx.controls;
    requires javafx.fxml;
    requires mysql.connector.java;
    requires java.sql;
    requires javafx.graphics;

    exports periodo_1;
    exports periodo_1.controller;
    exports periodo_1.model.enums;
    exports periodo_1.model.entity;
    exports periodo_1.model.dao;
}